import { mtof } from './util.js';

/**
 * FM kick sound.
 */
export default function createFMKick(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,
    cOscBuffer,

    init = function() {
      // record the kick sound and play it as a sample,
      // because scheduled oscillator start will not play the
      // waveform from its start point.
      let when = ctx.currentTime,
        numChannels = 1,
        length = 4 * ctx.sampleRate,
        offlineCtx = new OfflineAudioContext(numChannels, length, ctx.sampleRate),
        cOsc = offlineCtx.createOscillator(),
        m1Osc = offlineCtx.createOscillator(),
        m1Gain = offlineCtx.createGain(),
        m2Osc = offlineCtx.createOscillator(),
        m2Gain = offlineCtx.createGain();

      cOsc.connect(offlineCtx.destination);
      m1Osc.connect(m1Gain);
      m1Gain.connect(cOsc.frequency);
      m2Osc.connect(m2Gain);
      m2Gain.connect(offlineCtx.destination);

      cOsc.frequency.setValueAtTime(mtof(30), when);

      m1Osc.frequency.setValueAtTime(mtof(28), when);
      m1Gain.gain.setValueAtTime(2000, when);
      m1Gain.gain.exponentialRampToValueAtTime(0.0001, when + 0.1);

      m2Osc.type = 'sine';
      m2Osc.frequency.setValueAtTime(mtof(32), when);
      m2Gain.gain.setValueAtTime(2, when);
      m2Gain.gain.exponentialRampToValueAtTime(0.0001, when + 0.1);

      cOsc.start(when);
      m1Osc.start(when);
      m2Osc.start(when);

      offlineCtx.startRendering().then(function(renderedBuffer) {
        cOscBuffer = renderedBuffer;
      });
    },

    createVoice = function(when, level) {
      let osc = ctx.createBufferSource(),
        gain = ctx.createGain();

      osc.connect(gain);
      gain.connect(mixerInput);

      osc.buffer = cOscBuffer;
      osc.playbackRate.value = 1.0;
      gain.gain.setValueAtTime(level, when);
      gain.gain.exponentialRampToValueAtTime(0.00001, when + 1.2);

      osc.start(when);
      osc.stop(when + 1.5);

      osc.onended = function(e) {
        gain.disconnect();
        gain = null;
      };
    },

    play = function(when, index) {
      if (cOscBuffer) {
        for (let i = 0, n = 2; i < n; i++) {
          createVoice(when + (length * (i/2 + 1.25/32)), 0.6);
          createVoice(when + (length * (i/2 + 2.25/32)), 0.05);
        }
      }
    };

  init();

  return {
    play,
  }
};
