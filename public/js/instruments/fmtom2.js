/**
 * FM tom sound.
 */
export default function createFMTom2(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,
    output,
    filterFreq = 2000, // 783.991,

    /**
     * Create the output stage that voices are connected to.
     * Two filters in series form a 24dB bandpass filter,
     * followed by makeup gain node.
     */

    init = function() {
      let filter1 = ctx.createBiquadFilter(),
        filter2 = ctx.createBiquadFilter(),
        gain = ctx.createGain();

      filter1.connect(filter2);
      filter2.connect(gain);
      gain.connect(mixerInput);

      filter1.type = 'lowpass';
      filter1.frequency.value = filterFreq;
      filter1.Q.value = 3;

      filter2.type = 'lowpass';
      filter2.frequency.value = filterFreq;
      filter2.Q.value = 3;

      gain.gain.value = 1;

      output = filter1;
    },

    createVoice = function(when, duration, level = 1, freq = 1600) {
      let cOsc = ctx.createOscillator(),
        cGain = ctx.createGain(),
        m1Osc = ctx.createOscillator(),
        m1Gain = ctx.createGain(),
        m2Osc = ctx.createOscillator(),
        m2Gain = ctx.createGain();

      cOsc.connect(cGain);
      cGain.connect(output);
      m1Osc.connect(m1Gain);
      m1Gain.connect(cOsc.frequency);
      m2Osc.connect(m2Gain);
      m2Gain.connect(m1Osc.frequency);

      cOsc.frequency.setValueAtTime(freq / 8, when);
      cOsc.frequency.exponentialRampToValueAtTime(20, when + duration);
      cGain.gain.setValueAtTime(level, when);
      cGain.gain.exponentialRampToValueAtTime(0.0001, when + duration);

      m1Osc.frequency.setValueAtTime(freq, when);
      m1Osc.frequency.exponentialRampToValueAtTime(20, when + 0.04);
      m1Gain.gain.setValueAtTime(level * 500, when);
      m1Gain.gain.exponentialRampToValueAtTime(0.0001, when + duration);

      m2Osc.frequency.setValueAtTime(freq / 10, when);
      m2Osc.frequency.exponentialRampToValueAtTime(20, when + duration);
      m2Gain.gain.setValueAtTime(level * 1000, when);
      m2Gain.gain.exponentialRampToValueAtTime(0.0001, when + duration);

      cOsc.start(when);
      m1Osc.start(when);
      m2Osc.start(when);

      cOsc.stop(when + 1.0);

      cOsc.onended = function(e) {
        m1Osc.stop();
        m2Osc.stop();
        cGain.disconnect();
        cGain = null;
        m1Gain.disconnect();
        m1Gain = null;
        m2Gain.disconnect();
        m2Gain = null;
      };
    },

    play = function(when, index) {
      if (index >= 24 && index < 32) {
        return;
      }
      if (index >= 36 && index < 40) {
        return;
      }

      for (let i = 0, n = 2; i < n; i++) {
        if (index >= 8) {
          createVoice(when + (length * (i/2 + 9.0/32)), 0.3, 1, 1000);
        }
        if (index >= 8 || index % 2 === 1) {
          createVoice(when + (length * (i/2 + 14/32)), 0.3, 0.5, 2000);
        }
      }
    };

  init();

  return {
    play
  }
};
