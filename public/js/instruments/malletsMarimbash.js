import { dbtolin, mtof } from './util.js';

/**
 * Based on Ableton Operator Mallets-Marimbash setting.
 */
export default function createMalletsMarimbash(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,

    init = function() {
    },

    createVoice = function(when, until, oscFreq) {
      const oscALevel = dbtolin(-3.2);
      const envARelease = until;
      const oscBLevel = 500; // dbtolin(-19);
      const oscBFreqMult = 3.363;
      const envBAttack = 0.00558;
      const envBDecay = 0.2292;
      const envBSustain = 0.5;
      const envBRelease = until;

      const oscA = ctx.createOscillator();
      oscA.type = 'sine';
      oscA.frequency.setValueAtTime(oscFreq, when);

      const levelA = ctx.createGain();
      levelA.gain.setValueAtTime(oscALevel, when);

      const envA = ctx.createGain();
      envA.gain.setValueAtTime(1, when);
      envA.gain.setValueAtTime(1, until);
      envA.gain.exponentialRampToValueAtTime(0.0000001, until +  envARelease);

      const oscB = ctx.createOscillator();
      oscB.type = 'sine';
      oscB.frequency.setValueAtTime(oscFreq * oscBFreqMult, when);

      const levelB = ctx.createGain();
      levelB.gain.setValueAtTime(oscBLevel, when);

      const envB = ctx.createGain();
      envB.gain.setValueAtTime(0, when);
      envB.gain.linearRampToValueAtTime(1, when + envBAttack);
      envB.gain.setValueAtTime(1, when + envBAttack);
      envB.gain.exponentialRampToValueAtTime(envBSustain, when + envBAttack + envBDecay);
      envB.gain.setValueAtTime(envBSustain, when + envBAttack + envBDecay);
      // envB.gain.setValueAtTime(envBSustain, until);
      envB.gain.exponentialRampToValueAtTime(0.00000001, when + envBAttack + envBDecay + envBRelease);

      oscA.connect(levelA).connect(envA).connect(mixerInput);
      oscB.connect(levelB).connect(envB).connect(oscA.frequency);

      oscA.start(when);
      oscA.stop(until + envARelease);

      oscB.start(when);
      oscB.stop(until + envBRelease);
    },

    play = function(when, index) {
      createVoice(
            when + (length * (4/31)), 
            when + (length * (9/31)), 
            mtof(22));
      createVoice(
            when + (length * (14/31)), 
            when + (length * (15/31)), 
            mtof(29));
      // for (let i = 0; i < 8; i++) {
      //   createVoice(
      //     when + (length * ((i * 4)/32)), 
      //     when + (length * (((i * 4)+1)/32)), 
      //     mtof(24 + Math.round(Math.random() * 60)));
      // }
    };

  init();

  return {
    play
  }
};

