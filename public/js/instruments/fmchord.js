import { mtof } from './util.js';
import { getTuna } from './timer.js';

/**
 * FM chord.
 */
export default function createFMChord(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,
    voices = [
        {pitch:  0, modGain: null, panner: null},
        {pitch:  3, modGain: null, panner: null},
        {pitch:  7, modGain: null, panner: null},
        {pitch: 10, modGain: null, panner: null}],
    numVoices = voices.length,
    carGain,
    mainGain,

    init = function() {
      carGain = ctx.createGain();
      mainGain = ctx.createGain();

      for (var i = 0; i < numVoices; i++) {
          voices[i].modGain = ctx.createGain();
          voices[i].panner = ctx.createStereoPanner();
          voices[i].panner.connect(carGain);
      }

      mainGain.gain.value = 0.5;

      const tuna = getTuna();
      const delay = new tuna.Delay({
        feedback: 0.6,    //0 to 1+
        delayTime: length * 1000 * (15/32),    //1 to 10000 milliseconds
        wetLevel: 0.1,    //0 to 1+
        dryLevel: 1,       //0 to 1+
        cutoff: 800,      //cutoff frequency of the built in lowpass-filter. 20 to 22050
        bypass: 0,
      });

      carGain.connect(mainGain);
      mainGain.connect(delay).connect(mixerInput);
    },

    createVoice = function(when, until, freq, voiceIndex, loop_index) {
      let voice = voices[voiceIndex],
          modGain = voice.modGain,
          carOsc = ctx.createOscillator(),
          car2Osc = ctx.createOscillator(),
          modOsc = ctx.createOscillator(),
          loopIndex = 3;

      carOsc.connect(voice.panner);
      car2Osc.connect(voice.panner);
      modOsc.connect(modGain);
      modGain.connect(carOsc.frequency);
      modGain.connect(car2Osc.frequency);

      carOsc.frequency.value = freq;
      car2Osc.frequency.value = freq / 2;

      carGain.gain.setValueAtTime(0.02, when);
      carGain.gain.linearRampToValueAtTime(0.03, when + 0.01);
      carGain.gain.linearRampToValueAtTime(0.015, when + 0.5);
      carGain.gain.linearRampToValueAtTime(0.03, when + 0.98);
      carGain.gain.exponentialRampToValueAtTime(0.07, when + 1.0);

      modOsc.frequency.setValueAtTime(freq * 2, when);

      // second envelope point changes over time
      let triangle16LFO = ((loopIndex % 32) / 32 >= 0.5) ? ((16 - (loopIndex % 16)) / 16) : ((loopIndex % 16) / 16),
          triangle24LFO = 1, // ((loopIndex % 48) / 48 >= 0.5) ? ((24 - (loopIndex % 24)) / 24) : ((loopIndex % 24) / 24),
          envTime = ((loopIndex % 32) / 32) * 0.75,
          startLevel = 1 + (triangle24LFO * 1000),
          endLevel = 3000; // - (triangle24LFO * 2500);

      modGain.gain.setValueAtTime(startLevel, when);
      modGain.gain.exponentialRampToValueAtTime(500, when + 0.25 + envTime);
      modGain.gain.exponentialRampToValueAtTime(3000, when + 0.5);
      modGain.gain.exponentialRampToValueAtTime(endLevel, when + 1.0);

      let pan = 1 - (2 * (voiceIndex / (numVoices - 1))); // -1 + (2 * (voiceIndex / (numVoices - 1)));
      voice.panner.pan.setValueAtTime(pan, when);

      carOsc.start(when);
      car2Osc.start(when);
      modOsc.start(when);

      carOsc.stop(until);
      car2Osc.stop(until);
      modOsc.stop(until);
    },

    play = function(when, index) {
      for (var i = 0; i < numVoices; i++) {
        if ((index >= 0 && index < 48) && (index < 32 || index >= 42) && index % 4 !== 3) {
          createVoice(when + (length * (4/31 + 0.5)), when + (length * (5/31 + 0.5)), mtof(48 + voices[i].pitch), i, index);
        }
        if ((index < 16 || (index >= 42 && index < 56)) && index % 4 === 0) {
          createVoice(when + (length * (5.1/31 + 0.5)), when + (length * (6/31 + 0.5)), mtof(48 + voices[i].pitch), i, index);
        }
        if (index >= 40 && index % 4 === 1) {
          createVoice(when + (length * (15/31)), when + (length * (16/31)), mtof(60 + voices[i].pitch), i, index);
        }
      }
    };

  init();

  return {
    play
  }
};
