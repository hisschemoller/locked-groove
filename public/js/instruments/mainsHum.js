import { mtof } from './util.js';

/**
 * Mains hum sound.
 */
export default function createMainsHum(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,
    gain,
    filter,

    init = function() {
      gain = ctx.createGain();
      filter = ctx.createBiquadFilter();

      filter.connect(gain).connect(mixerInput);

      gain.gain.value = 0.1;

      filter.type = 'highpass';
      filter.frequency.value = 4000;
      filter.Q.value = 20;
    },

    createVoice = function(when, until, oscFreq, gainValue) {
      const osc = ctx.createOscillator();
      osc.type = 'sawtooth';
      osc.frequency.setValueAtTime(oscFreq, when);
      osc.connect(filter);
      osc.start(when);
      osc.stop(until);

      filter.frequency.setValueAtTime(oscFreq, when);
      gain.gain.setValueAtTime(gainValue, when);
    },

    play = function(when, index) {
      createVoice(when + (length * (12/31)), when + (length * (23/31)), 9000, 0.1);
      createVoice(when + (length * (3.25/31)), when + (length * (15/31)), 50, 0.02);
    };

  init();

  return {
    play
  }
};
