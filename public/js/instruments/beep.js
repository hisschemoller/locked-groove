import { mtof } from './util.js';

/**
 * Beep sound.
 */
export default function createBeep(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,
    osc,
    gain,
    hiBeepPosition = 0.875,
    hiBeepDisplacement = 0.25,

    init = function() {
      gain = ctx.createGain();
      gain.gain.value = 0;
      gain.connect(mixerInput);
      osc = ctx.createOscillator();
      osc.frequency.value = 440;
      osc.connect(gain);
      osc.start();
    },

    playBeep = function(when, length, loopIndex) {
      const newWhen = when + (length * (15/31));
      osc.frequency.setValueAtTime(mtof(63), newWhen);
      gain.gain.setValueAtTime(0.001, newWhen);
      gain.gain.exponentialRampToValueAtTime(0.5, newWhen + 0.008);
      gain.gain.setValueAtTime(0.5, newWhen + 0.1);
      gain.gain.exponentialRampToValueAtTime(0.001, newWhen + 0.12);

      // if (Math.random() > 0.2 && Math.random() < 0.4) {
          // when += (length * (4/16));
          // osc.frequency.setValueAtTime(mtof(36), when);
          // gain.gain.setValueAtTime(0.001, when);
          // gain.gain.exponentialRampToValueAtTime(0.5, when + 0.008);
          // gain.gain.setValueAtTime(0.5, when + 0.1);
          // gain.gain.exponentialRampToValueAtTime(0.001, when + 0.12);
      // }

      // if (Math.random() > 0.4 && Math.random() < 0.6) {
          // when += (length * (1/16));
          // osc.frequency.setValueAtTime(mtof(36), when);
          // gain.gain.setValueAtTime(0.001, when);
          // gain.gain.exponentialRampToValueAtTime(0.5, when + 0.008);
          // gain.gain.setValueAtTime(0.5, when + 0.1);
          // gain.gain.exponentialRampToValueAtTime(0.001, when + 0.12);
      // }

      var hiBeepWhen = when + (length * (15.6/31));
      // hiBeepPosition = (hiBeepPosition + hiBeepDisplacement) % length;
      // osc.frequency.value = mtof(120);
      // osc.frequency.setValueAtTime(mtof(120), hiBeepWhen);
      osc.frequency.exponentialRampToValueAtTime(mtof(60), hiBeepWhen);
      
      gain.gain.setValueAtTime(0.001, hiBeepWhen);
      gain.gain.exponentialRampToValueAtTime(0.01, hiBeepWhen + 0.008);
      gain.gain.setValueAtTime(0.01, hiBeepWhen + 0.3);
      gain.gain.exponentialRampToValueAtTime(0.01, hiBeepWhen + 2);

      // if (Math.random() > 0.9) {
          // var hiBeepWhen = when + (length * (13/14));
          // // hiBeepPosition = (hiBeepPosition + hiBeepDisplacement) % length;
          // osc.frequency.value = mtof(120);
          // osc.frequency.setValueAtTime(mtof(120), hiBeepWhen);
          // gain.gain.setValueAtTime(0.001, hiBeepWhen);
          // gain.gain.exponentialRampToValueAtTime(0.01, hiBeepWhen + 0.008);
          // gain.gain.setValueAtTime(0.01, hiBeepWhen + 0.3);
          // gain.gain.exponentialRampToValueAtTime(0.0000001, hiBeepWhen + 0.32);
    // }
    },

    play = function(when, index) {
      playBeep(when, length, index);
    };

  init();

  return {
    play
  }
};
