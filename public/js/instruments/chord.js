import { getBasicImpulseResponse, mtof } from '../util.js';
import { getTuna } from '../studio.js';

const LOW_VELOCITY = 0.3;
const CHORDS = [
  [
    { pitch:  45, velocity: 110,}, 
    { pitch:  50, velocity: 30, }, 
    { pitch:  57, velocity: 120, },
    { pitch:  64, velocity: 40, },
    { pitch:  65, velocity: 30, },
    { pitch:  69, velocity: 50, },
    { pitch:  71, velocity: 25, },
    { pitch:  76, velocity: 60, },
    { pitch:  83, velocity: 30, },
    { pitch:  84, velocity: 22, },
    { pitch:  88, velocity: 12, },
    { pitch:  90, velocity: 9, },
    { pitch:  95, velocity: 7, },
  ],
  [
    { pitch:  40, velocity: 127, }, 
    { pitch:  52, velocity: 64, }, 
    { pitch:  59, velocity: 61, },
    { pitch:  66, velocity: 74, },
    { pitch:  67, velocity: 50, },
    { pitch:  71, velocity: 85, },
    { pitch:  74, velocity: 47, },
    { pitch:  83, velocity: 70, },
    { pitch:  88, velocity: 14, },
    { pitch:  93, velocity: 17, },
    { pitch:  0, velocity: 0, },
    { pitch:  0, velocity: 0, },
    { pitch:  0, velocity: 0, },
  ],
  [
    { pitch:  40, velocity: 127 * LOW_VELOCITY, }, 
    { pitch:  52, velocity: 64 * LOW_VELOCITY, }, 
    { pitch:  59, velocity: 61 * LOW_VELOCITY, },
    { pitch:  66, velocity: 74 * LOW_VELOCITY, },
    { pitch:  67, velocity: 50 * LOW_VELOCITY, },
    { pitch:  71, velocity: 85 * LOW_VELOCITY, },
    { pitch:  74, velocity: 47 * LOW_VELOCITY, },
    { pitch:  83, velocity: 70 * LOW_VELOCITY, },
    { pitch:  88, velocity: 14 * LOW_VELOCITY, },
    { pitch:  93, velocity: 17 * LOW_VELOCITY, },
    { pitch:  0, velocity: 0, },
    { pitch:  0, velocity: 0, },
    { pitch:  0, velocity: 0, },
  ],
  [
    { pitch:  47, velocity: 28,}, 
    { pitch:  54, velocity: 40, }, 
    { pitch:  62, velocity: 70, },
    { pitch:  66, velocity: 57, },
    { pitch:  69, velocity: 63, },
    { pitch:  74, velocity: 38, },
    { pitch:  76, velocity: 31, },
    { pitch:  81, velocity: 33, },
    { pitch:  88, velocity: 19, },
    { pitch:  0, velocity: 0, },
    { pitch:  0, velocity: 0, },
    { pitch:  0, velocity: 0, },
    { pitch:  0, velocity: 0, },
  ],
  [
    { pitch:  48 , velocity: 28,}, 
    { pitch:  54, velocity: 40, }, 
    { pitch:  60 , velocity: 100, },
    { pitch:  64 , velocity: 57, },
    { pitch:  67 , velocity: 80, },
    { pitch:  72 , velocity: 0, },
    { pitch:  74 , velocity: 70, },
    { pitch:  79, velocity: 33, },
    { pitch:  88, velocity: 19, },
    { pitch:  0, velocity: 0, },
    { pitch:  0, velocity: 0, },
    { pitch:  0, velocity: 0, },
    { pitch:  0, velocity: 0, },
  ],
];
const PATTERNS = [
  [
    { chord: CHORDS[0], start: 0, end: 3, },
    { chord: CHORDS[1], start: 4, end: 6, },
    { chord: CHORDS[2], start: 6.01, end: 7, },
    { chord: CHORDS[3], start: 7.3, end: 15, },
  ],
  [
    { chord: CHORDS[0], start: 0, end: 3, },
    { chord: CHORDS[1], start: 4, end: 6, },
    { chord: CHORDS[2], start: 6.01, end: 7, },
    { chord: CHORDS[3], start: 7.3, end: 10, },
    { chord: CHORDS[4], start: 10.3, end: 13, },
  ],
];

/**
 * Chord sound.
 */
export default function createChord(specs) {
  
  const { ctx, mixerInput, } = specs;
  let convolver, convGain;
  let numVoices = 0;
  const voices = [];
    
  const init = function() {
    // convolver = ctx.createConvolver();
    // convolver.buffer = getBasicImpulseResponse(ctx, 2, 10);

    // convGain = ctx.createGain();
    // convGain.gain.value = 0.3;

    // const tuna = getTuna();
    // const delay = new tuna.Delay({
    //   feedback: 0.7, //0 to 1+
    //   delayTime: length * 1000 * (2/32), // 1 to 10000 milliseconds
    //   wetLevel: 0.7, // 0 to 1+
    //   dryLevel: 1, // 0 to 1+
    //   cutoff: 700, // cutoff frequency of the built in lowpass-filter. 20 to 22050
    //   bypass: 0,
    // });

    // convolver.connect(convGain).connect(delay).connect(mixerInput);

    // find largest chord
    numVoices = PATTERNS[0][0].chord.length;

    // create voices
    for (let i = 0; i < numVoices; i++) {
      const voice = {
        gain: ctx.createGain()
      };
      // voice.gain.connect(convolver);
      voice.gain.connect(mixerInput);
      voices.push(voice);
    }
  },

  /**
   * Called by the studio module when the next PATTERNS[0] is about to start.
   * @param {Number} when Next PATTERNS[0] start time.
   * @param {Number} duration Pattern duration in seconds.
   * @param {Number} index Voice index.
   */
  createVoice = (when, duration, index, pIndex) => {
    const voice = voices[index];

    // add new sine oscillator to the voice
    const osc = ctx.createOscillator();
    osc.type = 'sine';
    osc.connect(voice.gain);
    osc.start(when + (Math.random() * 0.2));
    osc.stop(when + duration - (Math.random() * 0.1));

    // fr
    const patternIndex = pIndex %  2;
    PATTERNS[patternIndex].forEach((chordData, chordIndex) => {
      const { start, end, chord, } = chordData;
      const { velocity, pitch } = chord[index];
      
      const frequency = mtof(pitch);
      const gain = Math.max(0.000001, velocity / 127);
      const timeDeviation = Math.random() * 0.3;

      if (chordIndex === 0) {
        osc.frequency.setValueAtTime(frequency, when + start);
        voice.gain.gain.setValueAtTime(gain, when + start);
      } else {
        osc.frequency.exponentialRampToValueAtTime(frequency, when + start + timeDeviation);
        voice.gain.gain.exponentialRampToValueAtTime(gain, when + start + timeDeviation);
      }

      if (chordIndex === PATTERNS[patternIndex].length - 1) {
        osc.frequency.setValueAtTime(frequency, when + end + timeDeviation);
        voice.gain.gain.setValueAtTime(0, when + end + timeDeviation);
      } else {
        osc.frequency.setValueAtTime(frequency, when + end + timeDeviation);
        voice.gain.gain.setValueAtTime(gain, when + end + timeDeviation);
      }
    });
  },

  // createVoiceOld = function(when, duration, voiceIndex, data, freq, gain) {
  //   const { pitch, velocity, p2, v2, } = data;
  //   const freq1 = mtof(pitch);
  //   const freq2 = mtof(p2);
  //   const whenChord2Rnd = Math.random() * 0.5;
  //   const whenChord2Start = when + 1 + whenChord2Rnd;
  //   const whenChord2End = when + 2 + whenChord2Rnd;

  //   let voice = voices[voiceIndex];
  //   voice.gain.gain.setValueAtTime(gain - (Math.random() * (gain * 0.5)), when);

  //   let osc = ctx.createOscillator();
  //   osc.type = 'sine';
  //   osc.frequency.setValueAtTime(freq1, when);
  //   osc.frequency.setValueAtTime(freq1, whenChord2Start);
  //   osc.frequency.exponentialRampToValueAtTime(freq2, whenChord2End);
  //   osc.connect(voice.gain);
  //   osc.start(when + (Math.random() * 0.2));
  //   osc.stop(when + duration - (Math.random() * 0.1));
  // },

  /**
   * Called by the studio module when the next PATTERNS[0] is about to start.
   * @param {Number} when Next PATTERNS[0] start time.
   * @param {Number} index Next PATTERNS[0] index.
   * @param {Number} duration Pattern duration in seconds.
   */
  play = function(when, index, duration) {
    voices.forEach((voice, i) => {
      createVoice(when, duration, i, index);
    });
  };

  init();

  return {
    play,
  }
};
