import { getNoise } from './util.js';
import { getTuna } from './timer.js';

/**
 * 808 kick sound.
 */
export default function createKick8(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,

    init = function() {
      
    },

    createVoice = function(when, duration = 2, kickIndex) {
      let osc, gain, clickOsc, clickGain, sawOsc, sawGain;

      osc = ctx.createOscillator();
      gain = ctx.createGain();
      clickOsc = ctx.createOscillator();
      clickGain = ctx.createGain();
      // sawOsc = ctx.createOscillator();
      // sawGain = ctx.createGain();

      osc.connect(gain).connect(mixerInput);
      clickOsc.connect(clickGain).connect(gain);
      // sawOsc.connect(sawGain).connect(mixerInput);

      osc.frequency.setValueAtTime(70, when);
      osc.frequency.exponentialRampToValueAtTime(32, when + 0.2);
      osc.frequency.exponentialRampToValueAtTime(24, when + duration);

      gain.gain.setValueAtTime(0.0001, when);
      gain.gain.exponentialRampToValueAtTime(1, when + 0.004);
      gain.gain.exponentialRampToValueAtTime(0.0001, when + duration);

      clickOsc.type = 'sawtooth';
      clickOsc.frequency.setValueAtTime(10, when);

      clickGain.gain.setValueAtTime(0.4, when);

      // sawOsc.type = 'sawtooth';
      // sawOsc.frequency.setValueAtTime(36, when);
      // sawOsc.frequency.exponentialRampToValueAtTime(36, when + duration);

      // const sawDuration = kickIndex % 2 == 0 ? 0.005 : duration/4;
      // const sawDuration = duration * 0.25 * Math.random();
      // sawGain.gain.setValueAtTime(0.2, when);
      // sawGain.gain.exponentialRampToValueAtTime(0.0001, when + sawDuration);

      osc.onended = function(e) {
        gain.disconnect();
        gain = null;
        clickGain.disconnect();
        clickGain = null;
        // sawGain.disconnect();
        // sawGain = null;
      };

      osc.start(when);
      osc.stop(when + duration);

      clickOsc.start(when);
      clickOsc.stop(when + 0.001);

      // sawOsc.start(when);
      // sawOsc.stop(when + duration);
    },

    play = function(when, index) {
      if (index >= 36 && index < 40) {
        return;
      }

      if (index >= 8) {
        for (let i = 0, n = 8; i < n; i++) {
          createVoice(when + (length * (i/8)), 0.6, i);
        }
      }
    };

  init();

  return {
    play
  }
};