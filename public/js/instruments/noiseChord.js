import { getNoise, mtof } from './util.js';

/**
 * Noise with tuned filters.
 * 
 * osc > panner > gain > filter
 */
export default function createNoiseChord(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,
    voice = {
      panner: null,
      gain: null,
      filter: null,
      lfoOsc: null,
      lfoGain: null,
    },
    voices = [{
        ...voice,
        pitch: -12,
      }, {
        ...voice,
        pitch: -2,
      }, {
        ...voice,
        pitch: -5,
      }, {
        ...voice,
        pitch: 7,
    }],
    numVoices = voices.length,
    gainValue = 5,

    init = function() {
      for (var i = 0; i < numVoices; i++) {
        let voice = voices[i];

        voice.panner = ctx.createStereoPanner();
        voice.gain = ctx.createGain();
        voice.filter = ctx.createBiquadFilter();
        voice.lfoOsc = ctx.createOscillator();
        voice.lfoGain = ctx.createGain();

        voice.filter.connect(voice.gain).connect(voice.panner).connect(mixerInput);
        voice.lfoOsc.connect(voice.lfoGain).connect(voice.gain.gain);

        voice.filter.type = 'bandpass';
        voice.filter.Q.value = 1000;

        voice.lfoOsc.type = 'sine';
        voice.lfoOsc.frequency.value = 1 / (length * (1 + (Math.random() * 1)));

        voice.lfoGain.gain.value = gainValue / 2;

        voice.lfoOsc.start();
      }
    },

    createVoice = function(when, until, voiceIndex, freq, pan, loopIndex) {
      let voice = voices[voiceIndex];
      voice.panner.pan.setValueAtTime(pan, when);
      voice.gain.gain.setValueAtTime(gainValue, when);
      voice.filter.frequency.setValueAtTime(freq, when);

      let osc = ctx.createBufferSource();
      osc.connect(voice.filter);
      osc.buffer = getNoise(ctx, 'white');
      osc.loop = true;
      osc.start(when, voiceIndex * 0.1);
      osc.stop(until);
    },

    play = function(when, index) {
      const repeatIndex = index % 32;

      let pitchAdd = 0;
      if (repeatIndex === 18) {
        pitchAdd = 12;
      } else if (repeatIndex === 19) {
        pitchAdd = 8;
      }

      let numVoicesNow = numVoices;
      if (index >= 32 && index < 40) {
        numVoicesNow = 1;
        pitchAdd = 12;
      }

      for (var i = 0; i < numVoicesNow; i++) {
        let pan = 1 - (2 * (i / (numVoicesNow - 1)));
        pan = numVoicesNow === 1 ? 0 : pan;
        createVoice(
          when + (length * (0/32)),
          when + (length * (32/32)), 
          i,
          mtof(60 + pitchAdd + voices[i].pitch), 
          pan,
          index);
      }
    };

  init();

  return {
    play
  }
};