/**
 * Click sound.
 */
export default function createClick(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,
    panner,
    buffer1,
    buffer6,

    init = function() {
      let bufferSize = 100;

      buffer1 = ctx.createBuffer(1, bufferSize, ctx.sampleRate);
      let bufferChannel = buffer1.getChannelData(0);
      for (let i = 0; i < bufferSize; i++) {
        bufferChannel[i] = (i > 10 && i < 12) ? 0.8 : 0;
      }

      buffer6 = ctx.createBuffer(1, bufferSize, ctx.sampleRate);
      bufferChannel = buffer6.getChannelData(0);
      for (let i = 0; i < bufferSize; i++) {
        bufferChannel[i] = (i > 10 && i < 17) ? 0.4 : 0;
      }

      panner = ctx.createStereoPanner();
      panner.connect(mixerInput);
    },

    createVoice = function(when, buffer) {
      let pan = (buffer === buffer6) ? -0.7 : 0.2 + (Math.random() * 0.5);
      panner.pan.setValueAtTime(pan, when);

      let src = ctx.createBufferSource();
      src.buffer = buffer;
      src.connect(panner);
      src.start(when);
    },

    play = function(when, index) {
      if (index >= 56 || (index >= 38 && index < 40)) {
        return;
      }

      for (let i = 0, len = 32; i < len; i++) {
        createVoice(when + (length * (i/len)), buffer1);
      }
      for (let i = 0, len = 31; i < len; i++) {
        createVoice(when + (length * (i/len)), buffer6);
      }
    };

  init();

  return {
    play
  }
};
