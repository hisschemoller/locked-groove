import { getNoise } from './util.js';
import { getTuna } from './timer.js';

/**
 * Click sound.
 */
export default function createNoise(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,
    panL,
    panR,
    gainOut,
    gain,

    init = function() {
      gainOut = ctx.createGain();
      panL = ctx.createStereoPanner();
      panR = ctx.createStereoPanner();

      const tuna = getTuna();
      const phaser = new tuna.Phaser({
        rate: 4,                     //0.01 to 8 is a decent range, but higher values are possible
        depth: 0.0,                    //0 to 1
        feedback: 0.6,                 //0 to 1+
        stereoPhase: 120,               //0 to 180
        baseModulationFrequency: 500,  //500 to 1500
        bypass: 0,
      });

      gain = 0.015;

      const delay = new tuna.Delay({
        feedback: 0.8,    //0 to 1+
        delayTime: 500,    //1 to 10000 milliseconds
        wetLevel: 0.9,    //0 to 1+
        dryLevel: 1,       //0 to 1+
        cutoff: 1600,      //cutoff frequency of the built in lowpass-filter. 20 to 22050
        bypass: 0,
      });

      panL.connect(gainOut);
      panR.connect(gainOut);
      gainOut.connect(phaser);
      phaser.connect(delay);
      delay.connect(mixerInput);

      panL.pan.value = -1.0;
      panR.pan.value = 1.0;
      gainOut.gain.value = gain;
    },

    createVoice = function(when, until) {
      let oscL = ctx.createBufferSource(),
        oscR = ctx.createBufferSource();

      oscL.connect(panL);
      oscR.connect(panR);

      oscL.buffer = getNoise(ctx, 'white');
      oscL.loop = true;
      oscR.buffer = getNoise(ctx, 'white');
      oscR.loop = true;

      gainOut.gain.setValueAtTime(0, when);
      gainOut.gain.linearRampToValueAtTime(gain, until);

      oscL.start(when);
      oscL.stop(until);
      oscR.start(when, 0.1);
      oscR.stop(until);

      oscL.onended = function(e) {};
    },

    play = function(when, index) {
      if (index % 32 === 12 || index % 32 === 20) {
        createVoice(when + (length * (10/31)), when + (length * (14/31)));
      }

      // createVoice(when + (length * (0/32)), when + (length * (4/32)));
    };

  init();

  return {
    play
  }
};
