/**
 * Hi-hat sound.
 */
export default function createHiHat(specs) {
  let ctx = specs.ctx,
    length = specs.length,
    mixerInput = specs.mixerInput,
    fundamental = 40,
    ratios = [2, 3, 4.16, 5.43, 6.79, 8.21],

    init = function() {
    },

    createVoice = function(when) {
      const volume = 1;
      const gain = ctx.createGain();
      const bandpass = ctx.createBiquadFilter();
      const highpass = ctx.createBiquadFilter();
      const oscs = ratios.map(ratio => {
          const osc = ctx.createOscillator();
          osc.type = 'square';
          osc.frequency.value = fundamental * ratio;
          osc.connect(bandpass);
          osc.start(when);
          osc.stop(when + 0.15 +  + (Math.random() * 0.1));
          return osc;
      });
                
      bandpass.type = 'bandpass';
      bandpass.frequency.value = 9500 + (Math.random() * 1000) + 2000;
      highpass.type = 'highpass';
      highpass.frequency.value = 6500 + (Math.random() * 1000) + 2000;
      gain.gain.setValueAtTime(0.00001, when);
      gain.gain.exponentialRampToValueAtTime(volume, when + 0.02);
      gain.gain.exponentialRampToValueAtTime(volume / 3, when + 0.03);
      gain.gain.exponentialRampToValueAtTime(0.00001, when + 0.25 + (Math.random() * 0.1) + 0.2)
            
      bandpass.connect(highpass).connect(gain).connect(mixerInput);
    },

    play = function(when, index) {
      for (let i = 0, n = 4; i < n; i++) {
        createVoice(when + (length * (i / 4)));
      }
    };

  init();

  return {
    play
  }
};
