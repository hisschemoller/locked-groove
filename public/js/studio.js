import { setup as setupClock, start as startClock } from './clock.js';
import { setup as setupMixer, addChannel as addMixerChannel } from './mixer.js';
import { setup as setupDisplay, show as showDisplay } from './display.js';

const instruments = [];
let ctx, tuna;

export function setup() {
  ctx = new (window.AudioContext || window.webkitAudioContext)();
  tuna = new Tuna(ctx);
  setupDisplay();
  setupMixer(ctx);
  setupClock(ctx);
}

export function addInstrument(instrCreateFunction, gain, pan, rvbSend) {
  const mixerChannel = addMixerChannel(gain, pan, rvbSend);
  const instrument = instrCreateFunction({ ctx, mixerInput: mixerChannel.getInput() });
  instruments.push(instrument);
}

/**
 * Called by the clock module when the next pattern is about to start.
 * @param {Number} startTime Next pattern start time.
 * @param {Number} loopIndex Next pattern index.
 * @param {Number} duration Pattern duration in seconds.
 */
export function onClock(startTime, loopIndex, duration) {
  instruments.forEach(instrument => {
    instrument.play(startTime, loopIndex, duration);
  });
  showDisplay(startTime, loopIndex);
}

export function getTuna() {
  return tuna;
}