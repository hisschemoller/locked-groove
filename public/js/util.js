/**
 * Create a basic reverb impulse response buffer,
 * which is logarithmic decaying noise.
 * @see http://stackoverflow.com/questions/34482319/web-audio-api-how-do-i-add-a-working-convolver
 * @param {Object} ctx WebAudioContext
 * @param {Number} duration [description]
 * @param {Number} decay    [description]
 * @param  {Boolean} reverse  [description]
 * @return {[type]}          [description]
 */
export function getBasicImpulseResponse(ctx, duration, decay, reverse) {
  let length = ctx.sampleRate * duration,
    impulse = ctx.createBuffer(2, length, ctx.sampleRate),
    impulseL = impulse.getChannelData(0),
    impulseR = impulse.getChannelData(1);

  decay = decay || 2.0;
  for (let i = 0; i < length; i++) {
    let n = reverse ? length - i : i;
    impulseL[i] = (Math.random() * 2 - 1) * Math.pow(1 - n / length, decay);
    impulseR[i] = (Math.random() * 2 - 1) * Math.pow(1 - n / length, decay);
  }
  return impulse;
}

function createBrownNoise(ctx, channels, bufferSize) {
  const buffer = ctx.createBuffer(channels, bufferSize, ctx.sampleRate);
  const bufferChannels = buffer.getChannelData(0);
  let i = 0;
  let white = 0;
  let lastOut = 0;

  for (i; i < bufferSize; i += 1) {
    white = Math.random() * 2 - 1;
    bufferChannels[i] = (lastOut + 0.02 * white) / 1.02;
    lastOut = bufferChannels[i];
    bufferChannels[i] *= 3.5; // (roughly) compensate for gain
  }

  return buffer;
}

function createPinkNoise(ctx, channels, bufferSize) {
  const buffer = ctx.createBuffer(channels, bufferSize, ctx.sampleRate);
  const bufferChannels = buffer.getChannelData(0);
  let white = 0;
  let i = 0;
  let b0 = 0;
  let b1 = 0;
  let b2 = 0;
  let b3 = 0;
  let b4 = 0;
  let b5 = 0;
  let b6 = 0;

  for (i = 0; i < bufferSize; i += 1) {
    white = Math.random() * 2 - 1;
    b0 = 0.99886 * b0 + white * 0.0555179;
    b1 = 0.99332 * b1 + white * 0.0750759;
    b2 = 0.969 * b2 + white * 0.153852;
    b3 = 0.8665 * b3 + white * 0.3104856;
    b4 = 0.55 * b4 + white * 0.5329522;
    b5 = -0.7616 * b5 - white * 0.016898;
    bufferChannels[i] = b0 + b1 + b2 + b3 + b4 + b5 + b6 + white * 0.5362;
    bufferChannels[i] *= 0.11;
    b6 = white * 0.115926;
  }

  return buffer;
}

function createWhiteNoise(ctx, channels, bufferSize) {
  const buffer = ctx.createBuffer(channels, bufferSize, ctx.sampleRate);
  const bufferChannels = buffer.getChannelData(0);

  for (let i = 0; i < bufferSize; i += 1) {
    bufferChannels[i] = Math.random() * 2 - 1;
  }

  return buffer;
}

let whiteNoise = null;
let pinkNoise = null;
let brownNoise = null;

export function getNoise(ctx, color) {
  const channels = 1;
  const bufferSize = ctx.sampleRate * 2;
  switch (color) {
    case 'white':
      if (!whiteNoise) {
        whiteNoise = createWhiteNoise(ctx, channels, bufferSize);
      }
      return whiteNoise;
    case 'pink':
      if (!pinkNoise) {
        pinkNoise = createPinkNoise(ctx, channels, bufferSize);
      }
      return pinkNoise;
    case 'brown':
      if (!brownNoise) {
        brownNoise = createBrownNoise(ctx, channels, bufferSize);
      }
      return brownNoise;
  }
}

/**
 * Converts a MIDI pitch number to frequency.
 * @param  {Number} midi MIDI pitch (0 ~ 127)
 * @return {Number} Frequency (Hz)
 */
export function mtof(midi) {
    if (midi <= -1500) return 0;
    else if (midi > 1499) return 3.282417553401589e+38;
    else return 440.0 * Math.pow(2, (Math.floor(midi) - 69) / 12.0);
}

/**
 * Converts frequency to MIDI pitch.
 * @param  {Number} freq Frequency
 * @return {Number}      MIDI pitch
 */
export function ftom(freq) {
  return Math.floor(
    freq > 0 ?
    Math.log(freq/440.0) / Math.LN2 * 12 + 69 : -1500
  );
};

/**
 * Converts power to decibel. Note that it is off by 100dB to make it
 *   easy to use MIDI velocity to change volume. This is the same
 *   convention that PureData uses. This behaviour might change in the
 *   future.
 * @param  {Number} power Power
 * @return {Number}       Decibel
 */
export function powtodb(power) {
  if (power <= 0) return 0;
  else {
    var db = 100 + 10.0 / Math.LN10 * Math.log(power);
    return db < 0 ? 0 : db;
  }
};

/**
 * Converts decibel to power. Note that it is off by 100dB to make it
 *   easy to use MIDI velocity to change volume. This is the same
 *   convention that PureData uses. This behaviour might change in the
 *   future.
 * @param  {Number} db Decibel
 * @return {Number}    Power
 */
export function dbtopow(db) {
  if (db <= 0) return 0;
  else {
    // TODO: what is 870?
    if (db > 870) db = 870;
    return Math.exp(Math.LN10 * 0.1 * (db - 100.0));
  }
};

/**
 * Converts RMS(root-mean-square) to decibel.
 * @param  {Number} rms RMS value
 * @return {Number}     Decibel
 */
export function rmstodb(rms) {
  if (rms <= 0) return 0;
  else {
    var db = 100 + 20.0 / Math.LN10 * Math.log(rms);
    return db < 0 ? 0 : db;
  }
};

/**
 * Converts decibel to RMS(root-mean-square).
 * @param  {Number} db  Decibel
 * @return {Number}     RMS value
 */
export function dbtorms(db) {
  if (db <= 0) return 0;
  else {
    // TO FIX: what is 485?
    if (db > 485) db = 485;
    return Math.exp(Math.LN10 * 0.05 * (db - 100.0));
  }
};

/**
 * Converts linear amplitude to decibel.
 * @param  {Number} lin Linear amplitude
 * @return {Number}     Decibel
 */
export function lintodb(lin) {
  // if below -100dB, set to -100dB to prevent taking log of zero
  return 20.0 * (lin > 0.00001 ? (Math.log(lin) / Math.LN10) : -5.0);
};

/**
 * Converts decibel to linear amplitude. Useful for dBFS conversion.
 * @param  {Number} db  Decibel
 * @return {Number}     Linear amplitude
 */
export function dbtolin(db) {
  return Math.pow(10.0, db / 20.0);
};

/**
 * Converts MIDI velocity to linear amplitude.
 * @param  {Number} velocity MIDI velocity
 * @return {Number}     Linear amplitude
 */
export function veltoamp(velocity) {
  // TODO: velocity curve here?
  return velocity / 127;
};
