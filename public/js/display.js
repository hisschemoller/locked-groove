let indexEl, timeEl;

export function setup() {
  indexEl = document.querySelector('.index');
  timeEl = document.querySelector('.time');
}

export function show(startTime, loopIndex) {
  indexEl.textContent = loopIndex;
  timeEl.textContent = startTime.toFixed(1);
}
