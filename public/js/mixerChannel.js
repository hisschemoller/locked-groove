
export default function createMixerChannel(specs) {
  let ctx = specs.ctx,
    gain,
    pan,
    reverbGain,

    init = function() {
      gain = ctx.createGain();
      pan = ctx.createStereoPanner();
      reverbGain = ctx.createGain();

      gain.gain.value = specs.gain;
      pan.pan.value = specs.pan;
      reverbGain.gain.value = specs.rvbGain;

      gain.connect(pan).connect(specs.dry);
      gain.connect(reverbGain).connect(specs.reverb);
    },
    
    getInput = function() {
      return gain;
    };
  
  init();

  return {
    getInput,
  };
}
