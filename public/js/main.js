import createChord from './instruments/chord.js';
import { addInstrument, setup as setupStudio } from './studio.js';
import { start as startClock } from './clock.js';

function go() {
  document.removeEventListener('click', go);
  setupStudio();
  addInstrument(createChord, 0.02, 0, 0);
  startClock(10);
}

document.addEventListener('DOMContentLoaded', function(e) {
  document.addEventListener('click', go);
});
