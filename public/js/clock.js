import { onClock } from './studio.js';

const PATTERN_DURATION_IN_SECS = 16;
const SONG_DURATION_IN_PATTERNS = 64;
let ctx, patternIndex, nextPatternStartTime;

export function setup(audioContext) {
  ctx = audioContext;
}

export function start(delay = 0) {
  setTimeout(function() {
    patternIndex = 0;
    nextPatternStartTime = ctx.currentTime;
    run();
  }, delay);
}

function run() {
  if (ctx.currentTime <= nextPatternStartTime && ctx.currentTime + 0.167 > nextPatternStartTime) {
      onClock(nextPatternStartTime, patternIndex, PATTERN_DURATION_IN_SECS);
      patternIndex += 1;
      nextPatternStartTime += PATTERN_DURATION_IN_SECS;
  }
  if (patternIndex < SONG_DURATION_IN_PATTERNS) {
    requestAnimationFrame(run);
  }
}
