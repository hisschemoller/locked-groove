import createMixerChannel from './mixerChannel.js';
import { getBasicImpulseResponse, mtof } from './util.js';

let channels = [],
  compressor,
  compressor2,
  ctx,
  dryGain,
  masterGain,
  reverb,
  reverbGain;


export function setup(audioContext) {
  ctx = audioContext;

  dryGain = ctx.createGain();

  reverb = ctx.createConvolver();
  reverbGain = ctx.createGain();
  compressor = ctx.createDynamicsCompressor();
  compressor2 = ctx.createDynamicsCompressor();

  reverb.buffer = getBasicImpulseResponse(ctx, 2, 10);
  reverbGain.gain.value = 0.5;

  compressor.threshold.setValueAtTime(-70, ctx.currentTime);
  compressor.knee.setValueAtTime(40, ctx.currentTime);
  compressor.ratio.setValueAtTime(20, ctx.currentTime);
  compressor.attack.setValueAtTime(0.03, ctx.currentTime);
  compressor.release.setValueAtTime(0.8, ctx.currentTime);

  compressor2.threshold.setValueAtTime(-10, ctx.currentTime);
  compressor2.knee.setValueAtTime(10, ctx.currentTime);
  compressor2.ratio.setValueAtTime(3, ctx.currentTime);
  compressor2.attack.setValueAtTime(0, ctx.currentTime);
  compressor2.release.setValueAtTime(0.25, ctx.currentTime);

  const makeupGain = ctx.createGain();
  makeupGain.gain.setValueAtTime(2, ctx.currentTime);

  masterGain = ctx.createGain();

  dryGain
  .connect(masterGain);

  reverb
    .connect(reverbGain)
    // .connect(compressor)
    // .connect(compressor2)
    // .connect(makeupGain)
    .connect(masterGain)
    .connect(ctx.destination);
}

/**
 * 
 */
export function addChannel(gain, pan, rvbGain = 0) {
  const mixerChannel = createMixerChannel({ ctx, dry: dryGain, reverb, gain, pan, rvbGain});
  channels.push(mixerChannel);
  return mixerChannel;
}
    
function start() {
  // fade in to compensate for initial compressor attack
  // outGain.gain.setValueAtTime(0.5, ctx.currentTime);
  // outGain.gain.exponentialRampToValueAtTime(1, ctx.currentTime + 0.02);
}
