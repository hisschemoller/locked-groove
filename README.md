# Locked Groove

Repeating web audio loops with multiple tempos, inspired by locked groove vinyl records and Steve Reich's 'It's Gonna Rain'.

